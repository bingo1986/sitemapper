package main

import (
	"bytes"
	"flag"
	"fmt"
	"github.com/jimsmart/grobotstxt"
	"golang.org/x/net/html"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
	"sync"
	"time"
)

// Outputter can be extended to provide various types of output
// for sitemaps
type Outputter interface {
	initializeOutput() Outputter
	writeToOutput(data []byte)
	closeOutput()
}

type robotChecker interface {
	isAllowed(pageURL string) bool
}

type textualOutputter struct {
	filename string
	file     *os.File
}

type gRobotChecker struct {
	txt string
}

type siteMapper struct {
	site        url.URL
	robots      robotChecker
	mappedPages []url.URL
	outputter   Outputter
	rateLimit int
}

func main() {
	start := time.Now()
	siteURL := flag.String("url", "", "site you want to map")
	rateLimit := flag.Int("ratelimit", 100, "Milliseconds to rate limit requests by")
	flag.Parse()

	outputter := NewTextualOutputter("map")
	siteMapper, err := NewSiteMapper(*siteURL, outputter, *rateLimit)

	if err != nil {
		log.Fatal(err)
	}

	siteMapper.mapSite()

	elapsed := time.Since(start)
	log.Printf("map finished in %s stored at map.txt", elapsed)
}

func (s *siteMapper) mapSite() {
	// Channel the outputter will listen on to output the sitemap
	outputCh := make(chan []byte)
	outputWg := &sync.WaitGroup{}
	s.outputter.initializeOutput()

	limiter := time.Tick(time.Duration(s.rateLimit) * time.Millisecond)

	outputWg.Add(1)
	s.mapPage(s.site, outputCh, outputWg, limiter)

	go func() {
		outputWg.Wait()
		close(outputCh)
	}()

	for i := range outputCh {
		s.outputter.writeToOutput(i)
	}

	s.outputter.closeOutput()
}

// NewSiteMapper will return an instance of a siteMapper to be used for
// mapping the given website. Output can be controlled via the outputter
func NewSiteMapper(siteURL string, outputter Outputter, rateLimit int) (*siteMapper, error) {
	siteMapper := siteMapper{}
	parsedSiteURL, err := url.ParseRequestURI(siteURL)

	if err != nil {
		return nil, fmt.Errorf("unable to create siteMapper invalid URL %s. Error: %s", siteURL, err)
	}

	siteMapper.site = *parsedSiteURL
	siteMapper.robots = createNewRobots(parsedSiteURL.String())
	siteMapper.outputter = outputter
	siteMapper.rateLimit = rateLimit

	return &siteMapper, nil
}

func (s *siteMapper) mapPage(pageURL url.URL, outputCh chan<- []byte, outputWg *sync.WaitGroup, limiter <-chan time.Time) {
	go func() {
		defer outputWg.Done()
		if s.robots.isAllowed(pageURL.String()) && !s.hasPageBeenMapped(pageURL) {
			s.addToMappedPages(pageURL)
			<-limiter
			body, err := getPageBody(pageURL.String())
			if err != nil {
				log.Printf("unable to get body %s", err)
				return
			}

			links, err := getLinksOnPage(body, pageURL)
			if err != nil {
				log.Printf("unable to get links %s", err)
				return
			}

			linksOutput := func() string {
				var linksAsString []string
				for _, link := range links {
					linksAsString = append(linksAsString, link.String())
				}
				return fmt.Sprintf("-- %s\n%s\n\n", pageURL.String(), strings.Join(linksAsString, "\n"))
			}()
			outputCh <- []byte(linksOutput)

			for _, page := range links {
				outputWg.Add(1)
				s.mapPage(page, outputCh, outputWg, limiter)
			}
		}
	}()
}

func (s *siteMapper) addToMappedPages(pageURL url.URL) {
	s.mappedPages = append(s.mappedPages, pageURL)
}

func (s *siteMapper) hasPageBeenMapped(pageURL url.URL) bool {
	for _, page := range s.mappedPages {
		if pageURL.Path == "" {
			pageURL.Path = "/"
		}
		if page.Path == "" {
			page.Path = "/"
		}
		if strings.TrimSuffix(pageURL.Path, "/") == strings.TrimSuffix(page.Path, "/") {
			return true
		}
	}
	return false
}

func (r *gRobotChecker) isAllowed(pageURL string) bool {
	return grobotstxt.AgentAllowed(r.txt, "sitemapper", pageURL)
}

func createNewRobots(siteURL string) robotChecker {
	body, err := getPageBody(fmt.Sprintf("%s/robots.txt", siteURL))

	if err != nil {
		// We set to blank as it's expected to allow anything
		// when no robots.txt is available
		return &gRobotChecker{txt: ""}
	}

	buf := new(bytes.Buffer)
	buf.ReadFrom(body)

	return &gRobotChecker{txt: buf.String()}
}

func getPageBody(pageURL string) (io.ReadCloser, error) {
	response, err := http.Get(pageURL)

	if err != nil {
		return nil, err
	}

	if response.StatusCode != 200 {
		return nil, fmt.Errorf("received non success response from page %s %v", pageURL, response.StatusCode)
	}

	return response.Body, nil
}

// NewTextualOutputter will return an instance of a
// textual outputter. This is used to write
// the sitemap output to a txt file.
func NewTextualOutputter(filename string) Outputter {
	return &textualOutputter{
		filename: filename,
	}
}

func (t *textualOutputter) initializeOutput() Outputter {
	newFile, err := os.Create(fmt.Sprintf("%s.txt", t.filename))

	if err != nil {
		log.Printf("Failed to create file")
	}

	t.file = newFile

	return t
}

func (t *textualOutputter) writeToOutput(data []byte) {
	_, err := t.file.Write(data)

	if err != nil {
		log.Printf("Failed to write to file %s", err)
	}
}

func (t *textualOutputter) closeOutput() {
	t.file.Close()
}

func getLinksOnPage(body io.ReadCloser, page url.URL) ([]url.URL, error) {
	var links []url.URL
	tokenizer := html.NewTokenizer(body)
	tokenType := tokenizer.Next()

	for !endOfPage(tokenType) {
		token := tokenizer.Token()

		if tokenType == html.StartTagToken && token.Data == "a" {
			href, err := getHref(token, page)
			if err != nil {
				log.Printf("unable to get href. %s", err)
				continue
			}
			if isLinkWanted(*href, page) {
				links = append(links, *href)
			}
		}

		tokenType = tokenizer.Next()
	}

	return links, nil
}

func isLinkWanted(href url.URL, site url.URL) bool {
	return removeWwwPrefix(href.Host) == removeWwwPrefix(site.Host)

}

func removeWwwPrefix(url string) string {
	return strings.Replace(url, "www.", "", 1)
}

func getHref(token html.Token, pageURL url.URL) (*url.URL, error) {
	for _, attribute := range token.Attr {
		if attribute.Key == "href" {
			href := attribute.Val
			if strings.HasPrefix(href, "/") {
				href = pageURL.Scheme + "://" + pageURL.Host + href
			}

			parsedHref, err := url.ParseRequestURI(href)

			if err != nil {
				return nil, err
			}

			return parsedHref, nil
		}
	}
	return nil, fmt.Errorf("unable to get href from token")
}

func endOfPage(tokenType html.TokenType) bool {
	return tokenType == html.ErrorToken
}
