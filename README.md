# SiteMapper

## Description
A crawler written in Go that will output a textual sitemap.
The tool makes use of Go routines to enable it to quickly crawl and
map multiple pages on a site at once. Rate limiting is put in place
as most sites will begin to throw errors without it.

## Usage

Clone project and run `go build .` to build the binary.
The tool has two available options on running `url`
and `ratelimit`. `url` is required and is used to provide the 
url to crawl and map, `rateliimit` is a time defined in millisecond
and is used to rate limit the speed of requests. The default is `100`.

``sitemapper -url=https://test.com -ratelimit=100``


