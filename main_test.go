package main

import (
	"bytes"
	"fmt"
	"golang.org/x/net/html"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"reflect"
	"sync"
	"testing"
	"time"
)

const homePagePath = "./testfiles/homepage.html"

type mockOutputter struct{}

func (m mockOutputter) initializeOutput() Outputter {
	return mockOutputter{}
}
func (m mockOutputter) writeToOutput(data []byte) {
	fmt.Printf(string(data))
}
func (m mockOutputter) closeOutput() {}

type mockRobotsChecker struct{}

func (r *mockRobotsChecker) isAllowed(pageURL string) bool {
	return pageURL == pageURL
}
func getNewMockRobots() robotChecker {
	return &mockRobotsChecker{}
}

func TestGetLinksOnPagesReturnsExpectedLinks(t *testing.T) {
	expectedLinks := []string{"https://test.com/1", "https://test.com/2", "https://test.com/3", "https://test.com/4"}
	var actualLinks []string

	body, err := getBody(homePagePath)

	errorOnUnexpectedError(t, err)

	site, _ := url.ParseRequestURI("https://test.com")
	links, err := getLinksOnPage(body, *site)

	errorOnUnexpectedError(t, err)

	for _, link := range links {
		actualLinks = append(actualLinks, link.String())
	}

	errorOnUnexpectedError(t, err)

	if reflect.DeepEqual(expectedLinks, actualLinks) == false {
		t.Errorf("actualLinks %v does not equal expectedLinks %v", actualLinks, expectedLinks)
	}
}

func TestIsLinkWanted(t *testing.T) {
	tests := []struct {
		link           string
		expectedResult bool
	}{{
		link:           "https://test.com/1",
		expectedResult: true,
	}, {
		link:           "https://facebook.com",
		expectedResult: false,
	}, {
		link:           "https://blog.test.com",
		expectedResult: false,
	}, {
		link:           "https://test.com/1",
		expectedResult: true,
	}, {
		link:           "https://www.test.com/1",
		expectedResult: true,
	}, {
		link:           "https://www.test.com/4",
		expectedResult: true,
	}}

	for _, test := range tests {
		firstURL, _ := url.ParseRequestURI("https://test.com")
		secondURL, _ := url.ParseRequestURI("https://www.test.com")

		for _, site := range []*url.URL{firstURL, secondURL} {
			link, _ := url.ParseRequestURI(test.link)
			result := isLinkWanted(*link, *site)
			if result != test.expectedResult {
				t.Errorf("actual result %v did not equal expected result %v for link %s and url %s", result, test.expectedResult, test.link, site.Host)
			}
		}
	}
}

func TestGetHref(t *testing.T) {
	tests := []struct {
		token         html.Token
		expectedHref  string
		expectedError string
	}{{
		token: html.Token{
			Type:     html.StartTagToken,
			DataAtom: 0,
			Data:     "page 1",
			Attr: []html.Attribute{{
				Namespace: "",
				Key:       "href",
				Val:       "https://test.com/page1",
			}},
		},
		expectedHref: "https://test.com/page1",
	}, {
		token: html.Token{
			Type:     html.StartTagToken,
			DataAtom: 0,
			Data:     "page 9",
			Attr:     nil,
		},
		expectedError: "unable to get href from token",
	}, {
		token: html.Token{
			Type:     html.StartTagToken,
			DataAtom: 0,
			Data:     "page 2",
			Attr: []html.Attribute{{
				Namespace: "",
				Key:       "href",
				Val:       "/page2",
			}},
		},
		expectedHref: "https://test.com/page2",
	}}

	pageURL, _ := url.ParseRequestURI("https://test.com/randompage")

	for _, test := range tests {
		actualHref, err := getHref(test.token, *pageURL)

		if test.expectedHref != "" && actualHref.String() != test.expectedHref {
			t.Errorf(
				"actual href [%s] did not match expected href [%s]",
				actualHref,
				test.expectedHref,
			)
		}

		if test.expectedError != "" && err.Error() != test.expectedError {
			t.Errorf(
				"actual error [%s] did not match expected error [%s]",
				err,
				test.expectedError,
			)
		}
	}
}

func TestEndOfPage(t *testing.T) {
	tests := []struct {
		tokenType     html.TokenType
		expectedValue bool
	}{{
		tokenType:     html.ErrorToken,
		expectedValue: true,
	}, {
		tokenType:     html.StartTagToken,
		expectedValue: false,
	}}

	for _, test := range tests {
		actualValue := endOfPage(test.tokenType)

		if actualValue != test.expectedValue {
			t.Errorf(
				"actual end of page value [%v] does not equal expected value [%v] for token type [%s]",
				actualValue,
				test.expectedValue,
				test.tokenType.String(),
			)
		}

	}
}

func TestTextualOutputterInatialzeOuput(t *testing.T) {
	outputter := NewTextualOutputter("test")
	outputter.initializeOutput()

	file, err := os.Open("test.txt")
	errorOnUnexpectedError(t, err)

	if file == nil {
		t.Errorf("Failed to create test file")
	}

	outputter.closeOutput()

	os.Remove("test.txt")
}

func TestTextualOutputterWrite(t *testing.T) {
	outputter := NewTextualOutputter("test")
	outputter.initializeOutput()
	line1 := "line 1\n"
	line2 := "line 2\n"

	outputter.writeToOutput([]byte(line1))
	outputter.writeToOutput([]byte(line2))
	outputter.closeOutput()

	fileContents, err := ioutil.ReadFile("test.txt")
	errorOnUnexpectedError(t, err)

	if string(fileContents) != line1+line2 {
		t.Errorf("File contents [%s] does not match given [%s]", string(fileContents), line1+line2)
	}

	os.Remove("test.txt")
}

func TestGetPageBody(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "<h1>test</h1>")
	}))
	defer ts.Close()

	body, err := getPageBody(ts.URL)
	errorOnUnexpectedError(t, err)
	defer body.Close()

	tokenizer := html.NewTokenizer(body)
	tokenizer.Next()
	token := tokenizer.Token()

	if token.Data != "h1" {
		t.Errorf("Unexpected body data")

	}
}

func TestCreateNewRobots(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "robotsChecker")
	}))
	defer ts.Close()

	robots := createNewRobots(ts.URL)

	if robots == nil {
		t.Errorf("Failed to create robots")
	}
}

func TestCreateNewRobotsTxtWorksWhenFailsToFindRobotsTxt(t *testing.T) {
	robots := createNewRobots("")

	if robots == nil {
		t.Errorf("Failed to create robots")
	}
}

func TestIsMapped(t *testing.T) {
	tests := []struct{
		mappedPage string
		newPage string
		hasBeenMapped bool
	}{{
		mappedPage:    "https://test.com",
		newPage:       "https://www.test.com/",
		hasBeenMapped: true,
	},{
		mappedPage:    "https://test.com/page1/",
		newPage:       "https://test.com/page1",
		hasBeenMapped: true,
	},{
		mappedPage:    "https://www.test.com/page1",
		newPage:       "https://test.com/page2",
		hasBeenMapped: false,
	}}
	siteMapper := siteMapper{}

	for _, test := range tests {
		mappedPage, _ := url.ParseRequestURI(test.mappedPage)
		newPage, _ := url.ParseRequestURI(test.newPage)
		siteMapper.mappedPages = append(siteMapper.mappedPages, *mappedPage)

		if siteMapper.hasPageBeenMapped(*newPage) != test.hasBeenMapped {
			t.Errorf("page %s should have matched %s", newPage.String(), mappedPage.String())
		}
	}
}

func TestNewSiteMapperReturnsSiteMapper(t *testing.T) {
	siteMapper, err := NewSiteMapper("https://test.com", mockOutputter{}, 100)
	errorOnUnexpectedError(t, err)

	if siteMapper == nil {
		t.Errorf("failed to create site mapper")
	}
}

func TestNewSiteMapperReturnsError(t *testing.T) {
	_, err := NewSiteMapper("test.com", mockOutputter{}, 100)

	if err.Error() != "unable to create siteMapper invalid URL test.com. Error: parse test.com: invalid URI for request" {
		t.Errorf("unexpected error returned %s", err)
	}
}

func TestMapPageOutputsAsExpected(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "<a href=\"/1\">link</a>")
	}))
	defer ts.Close()

	mockURL, _ := url.ParseRequestURI(ts.URL)

	outputCh := make(chan []byte)
	outputWg := &sync.WaitGroup{}
	s := siteMapper{
		site:        *mockURL,
		robots:      getNewMockRobots(),
		mappedPages: []url.URL{},
		outputter:   mockOutputter{},
	}

	limiter := time.Tick(10 * time.Millisecond)

	outputWg.Add(1)
	s.mapPage(s.site, outputCh, outputWg, limiter)

	go func() {
		outputWg.Wait()
		close(outputCh)
	}()

	for i := range outputCh {
		s.outputter.writeToOutput(i)
	}

	s.outputter.closeOutput()
}

func errorOnUnexpectedError(t *testing.T, err error) {
	if err != nil {
		t.Error(err)
	}
}

func getBody(htmlFilePath string) (io.ReadCloser, error) {
	htmlFile, err := ioutil.ReadFile(htmlFilePath)

	if err != nil {
		return nil, err
	}

	body := ioutil.NopCloser(bytes.NewBuffer(htmlFile))
	return body, nil
}
